using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public Camera cam;

    void Update()
    {
        GetComponent<Transform>().SetPositionAndRotation(cam.GetComponent<Transform>().position, cam.GetComponent<Transform>().rotation);
    }
}
