using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TMPro;

public class UIStateHandler : MonoBehaviour
{
    public RenderTexture rTexture;

    public GameObject prediction_text;

    public enum toolState
    {
        pen, eraser, delete
    };

    public toolState toolstate = toolState.pen;

    public void setPenActive()
    {
        toolstate = toolState.pen;
    }

    public void setEraserActive()
    {
        toolstate = toolState.eraser;
    }

    public void setDeleteActive()
    {
        toolstate = toolState.delete;
    }

    public void save()
    {
        StartCoroutine(saveCoroutine());
    }

    private async Task<string> sendPostRequest(byte[] raw_bytes)
    {
        HttpClient client = new HttpClient();

        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "http://127.0.0.1:5000");

        MultipartFormDataContent content = new MultipartFormDataContent();
        content.Add(new ByteArrayContent(raw_bytes), "bytes_image", Path.GetFileName("./image4.jpeg"));
        //content.Add(new ByteArrayContent(raw_bytes), "bytes_image");
        request.Content = content;

        HttpResponseMessage response = await client.SendAsync(request);
        response.EnsureSuccessStatusCode();
        string responseBody = await response.Content.ReadAsStringAsync();

        return responseBody;
    }

    private IEnumerator saveCoroutine()
    {
        yield return new WaitForEndOfFrame();
        RenderTexture.active = rTexture;
        Texture2D outputTexture = new Texture2D(rTexture.width, rTexture.height,TextureFormat.R8,false,false);
        
        outputTexture.ReadPixels(new Rect(0, 0, rTexture.width, rTexture.height), 0, 0);
        outputTexture.Apply();
              
        var raw_bytes = outputTexture.GetRawTextureData();
        var task = sendPostRequest(raw_bytes);
        yield return new WaitUntil(() => task.IsCompleted);
        var text=JsonConvert.DeserializeObject<Dictionary<string,string>>(task.Result);
        prediction_text.GetComponent<TextMeshProUGUI>().text=text["result"];
        Debug.Log(text);

        //var dirPath = "C:/Users/Asus/SaveImages/";
        //if (!Directory.Exists(dirPath))
        //{
        //    Debug.Log("created new folder");
        //    Directory.CreateDirectory(dirPath);
        //}
        //File.WriteAllBytes(dirPath + "imagebytes", raw_bytes);
    }
}
