using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class DrawManager : MonoBehaviour
{
    public ComputeShader computeshader;
    public UIStateHandler uiHandler;
    public Slider mainSlider;


    private RenderTexture canvasRenderTexture;
    private int sizex = 1080;
    private int sizey = 1080;


    [SerializeField] Color backgroundColour = Color.white;
    [SerializeField] Color brushColour = Color.red;
    [SerializeField] float brushSize = 100;
    [SerializeField, Range(0.01f, 1)] float interpolationInterval = 0.01f;


    private int kernelId;

    Vector3 pm_pos;
    Vector3 m_pos;

    // Vector4 previousMousePosition;

    // Start is called before the first frame update
    void Start()
    {
        mainSlider.SetValueWithoutNotify(0.5f);
        canvasRenderTexture = new RenderTexture(sizex, sizey, 24);
        canvasRenderTexture.enableRandomWrite = true;
        canvasRenderTexture.filterMode = FilterMode.Point;
        canvasRenderTexture.Create();

        int initBackgroundKernel = computeshader.FindKernel("InitBackground");

        GetComponent<Renderer>().material.SetTexture("_MainTex", canvasRenderTexture);

        computeshader.SetVector("BackgroundColour", backgroundColour);
        computeshader.SetTexture(initBackgroundKernel, "Canvas", canvasRenderTexture);
        computeshader.SetFloat("CanvasWidth", canvasRenderTexture.width);
        computeshader.SetFloat("CanvasHeight", canvasRenderTexture.height);
        computeshader.GetKernelThreadGroupSizes(initBackgroundKernel,
            out uint xGroupSize, out uint yGroupSize, out _);
        computeshader.Dispatch(initBackgroundKernel,
            Mathf.CeilToInt(canvasRenderTexture.width / (float)xGroupSize),
            Mathf.CeilToInt(canvasRenderTexture.height / (float)yGroupSize),
            1);
                    pm_pos=Vector3.positiveInfinity;

        // previousMousePosition = Input.mousePosition;

    }

    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        switch (uiHandler.toolstate)
        {
            case UIStateHandler.toolState.pen:
                handleDrawing(brushColour);
                break;

            case UIStateHandler.toolState.eraser:
                handleDrawing(backgroundColour);
                break;

            case UIStateHandler.toolState.delete:
                if (Input.GetMouseButton(0))
                    handleDeleting();
                break;
        }
    }
    // private Vector3 m_pos, pm_pos;
    private void handleDrawing(Color brushcolor)
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            // var expected = new Vector2(canvasRenderTexture.width * Input.mousePosition.x / Screen.width, canvasRenderTexture.height * Input.mousePosition.y / Screen.height);

            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.transform.tag == "main_plane")
                {

                    this.m_pos = new Vector3(canvasRenderTexture.width * (1 - hit.textureCoord.x), canvasRenderTexture.height * (1 - hit.textureCoord.y), 0);
                    if (pm_pos == Vector3.positiveInfinity)                    
                        pm_pos = m_pos;


                    int updateKernel = computeshader.FindKernel("HandleDraw");
                    computeshader.SetVector("PreviousMousePosition", this.pm_pos);
                    computeshader.SetVector("MousePosition", this.m_pos);
                    computeshader.SetBool("MouseDown", Input.GetMouseButton(0));
                    computeshader.SetFloat("BrushSize", brushSize * mainSlider.value);
                    computeshader.SetVector("BrushColour", brushcolor);
                    computeshader.SetFloat("InterpolationInterval", interpolationInterval);

                    computeshader.SetTexture(updateKernel, "Canvas", canvasRenderTexture);
                    computeshader.SetFloat("CanvasWidth", canvasRenderTexture.width);
                    computeshader.SetFloat("CanvasHeight", canvasRenderTexture.height);

                    computeshader.GetKernelThreadGroupSizes(updateKernel,
                        out uint xGroupSize, out uint yGroupSize, out _);
                    computeshader.Dispatch(updateKernel,
                        Mathf.CeilToInt(canvasRenderTexture.width / (float)xGroupSize),
                        Mathf.CeilToInt(canvasRenderTexture.height / (float)yGroupSize),
                        1);
                    // previousMousePosition = Input.mousePosition;
                                        pm_pos = m_pos;


                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            pm_pos = Vector3.positiveInfinity;
        }

    }

    private void handleDeleting()
    {
        Ray mouseray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseray, out hit))
        {
            Destroy(hit.transform.gameObject);
        }
    }

}
